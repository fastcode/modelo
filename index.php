<?php

define('START_EXECUTION', microtime(true));

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("inc/configuration.php");
require_once("inc/class/vendor/Slim/vendor/autoload.php");

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);
//Create Slim
$app->get("/info", function(){
    phpinfo();
});
$modules_path = $_SERVER["DOCUMENT_ROOT"]."/gail/modules/";
//
    foreach (scandir($modules_path) as $file) {
        if ($file !== '.' && $file !== '..') {        
            require_once($modules_path.$file);        
        }
    }

$app->run();

?>
