<?php 

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get("/", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Dashboard"
    	));
    $page->setTpl("index");
});

/*******************
	DASHBOARD
*******************/
$app->get("/home", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Dashboard"
    	));
    $page->setTpl("index");
});

/*******************
	ORAÇAMENTOS
*******************/
$app->get("/orcamentos", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Orçamentos",
    	"singular"=>"orçamento",
		"novo"=>"novoorcamento",
		"contato"=>"Julio Cesar",
		"empresa"=>"Empresa J",
		"desc"=>"Ut turpis urna, tristique sed adipiscing nec, luctus quis leo. Fusce nec volutpat ante.",
		"dtalter"=>"15/03/2017 - 16:24",
		"valor"=>"R$ 7.834,56",
		"endereco"=>"Rua D, 67 - Centro - São Paulo - SP",
		"status"=>"Aberto",
		"footer"=>"Expira em 2 dias",
		"cor"=>"success"
    	));
    $page->setTpl("card");
});



/*******************
	OBS: POSSIBILIDADES PARA O ORÇAMENTO
*******************/
$app->get("/xorcamentos", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Possibilidades para Orçamentos"
    	));
    $page->setTpl("orcamentos");
});



/*******************
	NOVO ORAÇAMENTOS
*******************/
$app->get("/novoorcamento", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Novo orçamento"
    	));
    $page->setTpl("novoorcamento");
});


/*******************
	PEDIDOS
*******************/
$app->get("/pedidos", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Pedidos",
		"singular"=>"pedido",
		"novo"=>"novopedido",
		"contato"=>"Julio Cesar",
		"empresa"=>"Empresa J",
		"desc"=>"Ut turpis urna, tristique sed adipiscing nec, luctus quis leo. Fusce nec volutpat ante.",
		"dtalter"=>"15/03/2017 - 16:24",
		"valor"=>"R$ 7.834,56",
		"endereco"=>"Rua D, 67 - Centro - São Paulo - SP",
		"status"=>"Aberto",
		"footer"=>"Faltam 2 dias para o prazo",
		"cor"=>"success"
    	));
    $page->setTpl("card");
});

/*******************
	NOVO PEDIDOS
*******************/

$app->get("/novopedido", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Novo pedido"
    	));
    $page->setTpl("novopedido");
});


/*******************
	CLIENTES
*******************/
$app->get("/clientes", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Clientes",
		"singular"=>"cliente",
		"item1-tp"=>"numeric",
		"item1"=>"CNPJ",
		"item2-tp"=>"alpha",
		"item2"=>"Razão Social",
		"item3-tp"=>"alpha",
		"item3"=>"Responsável",
		"item4-tp"=>"numeric",
		"item4"=>"Tipo",
		"item5-tp"=>"alpha",
		"item5"=>"Local",
		"dado1a"=>"123345239074",
		"dado1b"=>"Empresa A",
		"dado1c"=>"Lucas",
		"dado1d"=>"PJ",
		"dado1e"=>"Jacareí/SP"
    	));
    $page->setTpl("listagem");
});
$app->get("/novocliente", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Novo Cliente"
    	));
    $page->setTpl("novocliente");
});

/*******************
	PRODUTOS
*******************/
$app->get("/produtos", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Produtos",
		"titulo"=>"Item 1",
		"cod"=>"7653425",
		"peso"=>"390",
		"colecao"=>"Coleção1",
		"st"=>"Sim"
    	));
    $page->setTpl("produtos");
});


/*******************
	TRANSPORTADORA
*******************/
$app->get("/transportadoras", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Transportadoras",
		"singular"=>"transportadora",
		"item1-tp"=>"numeric",
		"item1"=>"CNPJ",
		"item2-tp"=>"alpha",
		"item2"=>"Nomel",
		"item3-tp"=>"alpha",
		"item3"=>"Email",
		"item4-tp"=>"numeric",
		"item4"=>"Telefone",
		"item5-tp"=>"alpha",
		"item5"=>"Local",
		"dado1a"=>"123345239074",
		"dado1b"=>"Empresa A",
		"dado1c"=>"lucas@empresaa.com",
		"dado1d"=>"1133333333",
		"dado1e"=>"Jacareí/SP"
    	));
    $page->setTpl("listagem");
});


/*******************
	OBRAS
*******************/
$app->get("/obras", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Obras",
		"singular"=>"obra",
		"item1-tp"=>"alpha",
		"item1"=>"Nome",
		"item2-tp"=>"alpha",
		"item2"=>"Tipo",
		"item3-tp"=>"alpha",
		"item3"=>"Fase",
		"item4-tp"=>"numeric",
		"item4"=>"CEP",
		"item5-tp"=>"alpha",
		"item5"=>"Local",
		"dado1a"=>"Shopping Center Norte",
		"dado1b"=>"Tipo 1",
		"dado1c"=>"Estudo e viabilidade",
		"dado1d"=>"03234-200",
		"dado1e"=>"São Paulo/SP"
    	));
    $page->setTpl("listagem");
});


/*******************
	Perfil
*******************/
$app->get("/perfil", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Perfil",
		"nome"=>"Guilherme Moreira",
		"funcao"=>"Expecificador",
		"id"=>"17645",
		"desc"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis id, est voluptatibus numquam, voluptas doloremque sit commodi, voluptate sint aperiam illum nemo voluptates! Reprehenderit quos officiis mollitia accusantium natus vitae.",
		"dtnasc"=>"01/04/1982",
		"admissao"=>"15/04/1999",
		"email"=>"gui@gmail.com",
		"fone"=>"11998782435",
		"regiao"=>"Direto",
		"alcada"=>"15%"
    	));
    $page->setTpl("perfil");
});

/*******************
	Avisos
*******************/
$app->get("/avisos", function () use ($app) {
    $page = new Page(array(
    	"head_title"=>"Avisos",
		"nome"=>"Guilherme Moreira"
    	));
    $page->setTpl("mensagens");
});





?>