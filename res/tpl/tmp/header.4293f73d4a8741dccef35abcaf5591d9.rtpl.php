<?php if(!class_exists('raintpl')){exit;}?><!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  	<meta charset="UTF-8" />
  	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
	<meta http-equiv="Cache-control" content="public">
	<META HTTP-EQUIV="Expires" CONTENT="-1">
	<title><?php echo $head_title;?></title>
	<meta name="description" content="<?php echo $head_description;?>" />
	<meta name="keywords" content="<?php echo $head_keywords;?>" />
	<meta name="author" content="fasters.com.br" />
	<meta property="og:type" content="website">
	<meta property="og:locale" content="pt_BR">
	<meta property="og:url" content="<?php echo $head_url;?>">
 	<meta property="og:title" content="<?php echo $head_title;?>">
	<meta property="og:description" content="<?php echo $head_description;?>">
	<link rel="canonical" href="<?php echo $url;?>" />
	<link rel="shortcut icon" href="favicon.ico">
	
	<link rel="stylesheet" href="<?php echo $path;?>res/css/style.css" media="all" />
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/animate.css/animate.min.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/bootstrap/dist/css/bootstrap.min.css" media="all" />
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/owl/dist/assets/owl.carousel.min.css" media="all" />
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/alertify/css/alertify.min.css" />
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/ionicons/css/ionicons.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/blueimp-gallery/css/blueimp-gallery.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/blueimp-gallery/css/blueimp-gallery-indicator.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/blueimp-gallery/css/blueimp-gallery-video.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/rickshaw/rickshaw.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/select2/dist/css/select2.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/clockpicker/dist/bootstrap-clockpicker.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/nouislider/distribute/nouislider.min.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/summernote/dist/summernote.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/dropzone/dist/basic.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/dropzone/dist/dropzone.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/jquery.bootgrid/dist/jquery.bootgrid.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/datatables/media/css/jquery.dataTables.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/loaders.css/loaders.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/ng-material-floating-button/mfb/dist/mfb.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/material-colors/dist/colors.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/css/app.css" media="all" />
	<!--[if lt IE 9]>
	  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<SCRIPT TYPE="text/javascript">
		var initJQ = [];
	</SCRIPT>
</head>

<body class="theme-1">
	<div class="layout-container">
	 <!-- top navbar-->
      <header class="header-container">
        <nav>
          <ul class="visible-xs visible-sm">
            <li><a id="sidebar-toggler" href="#" class="menu-link menu-link-slide"><span><em></em></span></a></li>
          </ul>
          <ul class="hidden-xs">
            <li><a id="offcanvas-toggler" href="#" class="menu-link menu-link-slide"><span><em></em></span></a></li>
          </ul>
          <h2 class="header-title"><?php echo $head_title;?></h2>
          <ul class="pull-right">
           <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle has-badge ripple"><em class="ion-plus-circled"></em></a>
           <ul class="dropdown-menu dropdown-menu-right md-dropdown-menu">
                <li class="text-center"><a href="novoorcamento"><em class="ion-chatbox-working icon-fw text-info"></em>Orçamento</a></li>
                <li class="text-center"><a href="novopedido"><em class="ion-clipboard icon-fw text-info"></em>Pedido</a></li>
              </ul>
            </li>
            <li><a id="header-search" href="#" class="ripple"><em class="ion-ios-search-strong"></em></a></li>
            <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle has-badge ripple"><em class="ion-person"></em><sup class="badge bg-danger">3</sup></a>
              <ul class="dropdown-menu dropdown-menu-right md-dropdown-menu">
                <li><a href="perfil"><em class="ion-home icon-fw"></em>Perfil</a></li>
                <li><a href="avisos"><em class="ion-gear-a icon-fw"></em>Avisos</a></li>
                <li role="presentation" class="divider"></li>
                <li><a href="user.login.html"><em class="ion-log-out icon-fw"></em>Sair</a></li>
              </ul>
            </li>
            <!--<li><a id="header-settings" href="#" class="ripple"><em class="ion-gear-b"></em></a></li>-->
          </ul>
        </nav>
      </header>
      <?php $tpl = new RainTPL;$tpl->assign( $this->var );$tpl->draw( "sidebar" );?>

      <main class="main-container">