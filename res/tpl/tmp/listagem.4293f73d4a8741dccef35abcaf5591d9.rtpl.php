<?php if(!class_exists('raintpl')){exit;}?><section>
          <div class="container-fluid">
            <!-- DATATABLE DEMO 1-->
            <div class="card">
              <div class="card-heading">
            <div class="col-lg-8 col-md-8 col-sm-8">
              <div class="card-title"><?php echo $head_title;?></div>
              <div class="text-muted"></div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4"> <a href="#">
              <button type="button" class="fw btn btn-default ripple text-info"><strong><em class="ion-plus-circled"></em> Adicionar <?php echo $singular;?></strong></button>
              </a> </div>
            <br><br>
          </div>
              <div class="card-body">
                <table id="datatable1" class="table-datatable table table-striped table-hover mv-lg">
                  <thead>
                    <tr>
                      <th class="sort-<?php echo $item1-tp;?>"><?php echo $item1;?></th>
                      <th class="sort-<?php echo $item2-tp;?>"><?php echo $item2;?></th>
                      <th class="sort-<?php echo $item3-tp;?>"><?php echo $item3;?></th>
                      <th class="sort-<?php echo $item4-tp;?>"><?php echo $item4;?></th>
                      <th class="sort-<?php echo $item5-tp;?>"><?php echo $item5;?></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                    <tr class="gradeX">
                      <td><?php echo $dado1a;?></td>
                      <td><?php echo $dado1b;?></td>
                      <td><?php echo $dado1c;?></td>
                      <td><?php echo $dado1d;?></td>
                      <td><?php echo $dado1e;?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>