<?php if(!class_exists('raintpl')){exit;}?> <section>
          <div class="container container-md">
            <p class="ph">Hoje</p>
            <div class="card">
              <table class="table table-hover table-fixed va-middle">
                <tbody>
                  <tr class="msg-display clickable">
                    <td class="wd-xxs">
                      <div class="initial32 bg-info">G</div>
                    </td>
                    <td class="text-ellipsis wd-sm">Gail</td>
                    <td class="text-ellipsis">Fiquem atentos aos valores dos produtos. Dia 23 de março de 2017 todas as coleções sofrerão um aumento de 5% no valor líquido.</td>
                    <td class="wd-xxs">
                      <!-- START dropdown-->
                      <div class="pull-right dropdown">
                        <button type="button" data-toggle="dropdown" class="btn btn-default btn-flat btn-flat-icon"><em class="ion-android-more-vertical"></em></button>
                        <ul role="menu" class="dropdown-menu md-dropdown-menu dropdown-menu-right">
                          <li><a href="#"><em class="ion-trash-a icon-fw"></em>Apagar</a></li>
                        </ul>
                      </div>
                      <!-- END dropdown-->
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            
            
            <p class="ph">Ontem</p>
            <div class="card">
              <table class="table table-hover table-fixed va-middle">
                <tbody>
                  <tr class="msg-display clickable">
                    <td class="wd-xxs">
                      <div class="initial32 bg-info">T</div>
                    </td>
                    <td class="text-ellipsis wd-sm">Timmothy Terry</td>
                    <td class="text-ellipsis">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris hendrerit nibh eu nisi congue dapibus.</td>
                    <td class="wd-xxs">
                      <!-- START dropdown-->
                      <div class="pull-right dropdown">
                        <button type="button" data-toggle="dropdown" class="btn btn-default btn-flat btn-flat-icon"><em class="ion-android-more-vertical"></em></button>
                        <ul role="menu" class="dropdown-menu md-dropdown-menu dropdown-menu-right">
                          <li><a href="#"><em class="ion-reply icon-fw"></em>Reply</a></li>
                          <li><a href="#"><em class="ion-forward icon-fw"></em>Forward</a></li>
                          <li><a href="#"><em class="ion-trash-a icon-fw"></em>Spam</a></li>
                        </ul>
                      </div>
                      <!-- END dropdown-->
                    </td>
                  </tr>
                  <tr class="msg-display clickable">
                    <td>
                      <div class="initial32 bg-info">N</div>
                    </td>
                    <td class="text-ellipsis">Nina Brown</td>
                    <td class="text-ellipsis">Etiam eu ipsum dui, et tincidunt orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</td>
                    <td>
                      <!-- START dropdown-->
                      <div class="pull-right dropdown">
                        <button type="button" data-toggle="dropdown" class="btn btn-default btn-flat btn-flat-icon"><em class="ion-android-more-vertical"></em></button>
                        <ul role="menu" class="dropdown-menu md-dropdown-menu dropdown-menu-right">
                          <li><a href="#"><em class="ion-reply icon-fw"></em>Reply</a></li>
                          <li><a href="#"><em class="ion-forward icon-fw"></em>Forward</a></li>
                          <li><a href="#"><em class="ion-trash-a icon-fw"></em>Spam</a></li>
                        </ul>
                      </div>
                      <!-- END dropdown-->
                    </td>
                  </tr>
                  <tr class="msg-display clickable">
                    <td>
                      <div class="initial32 bg-gray-light">T</div>
                    </td>
                    <td class="text-ellipsis">Ted Vasquez</td>
                    <td class="text-ellipsis">Praesent vel nisi nibh.Integer imperdiet, turpis nec viverra egestas, magna sapien convallis velit, nec vestibulum turpis dui vel dui.</td>
                    <td>
                      <!-- START dropdown-->
                      <div class="pull-right dropdown">
                        <button type="button" data-toggle="dropdown" class="btn btn-default btn-flat btn-flat-icon"><em class="ion-android-more-vertical"></em></button>
                        <ul role="menu" class="dropdown-menu md-dropdown-menu dropdown-menu-right">
                          <li><a href="#"><em class="ion-reply icon-fw"></em>Reply</a></li>
                          <li><a href="#"><em class="ion-forward icon-fw"></em>Forward</a></li>
                          <li><a href="#"><em class="ion-trash-a icon-fw"></em>Spam</a></li>
                        </ul>
                      </div>
                      <!-- END dropdown-->
                    </td>
                  </tr>
                  <tr class="msg-display clickable">
                    <td>
                      <div class="initial32 bg-gray-light">L</div>
                    </td>
                    <td class="text-ellipsis">Lillian Snyder</td>
                    <td class="text-ellipsis">Praesent vel nisi nibh.Integer imperdiet, turpis nec viverra egestas, magna sapien convallis velit, nec vestibulum turpis dui vel dui.</td>
                    <td>
                      <!-- START dropdown-->
                      <div class="pull-right dropdown">
                        <button type="button" data-toggle="dropdown" class="btn btn-default btn-flat btn-flat-icon"><em class="ion-android-more-vertical"></em></button>
                        <ul role="menu" class="dropdown-menu md-dropdown-menu dropdown-menu-right">
                          <li><a href="#"><em class="ion-reply icon-fw"></em>Reply</a></li>
                          <li><a href="#"><em class="ion-forward icon-fw"></em>Forward</a></li>
                          <li><a href="#"><em class="ion-trash-a icon-fw"></em>Spam</a></li>
                        </ul>
                      </div>
                      <!-- END dropdown-->
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <p class="ph">2 days ago</p>
            <div class="card">
              <table class="table table-hover table-fixed va-middle">
                <tbody>
                  <tr class="msg-display clickable">
                    <td class="wd-xxs">
                      <div class="initial32 bg-gray-light">T</div>
                    </td>
                    <td class="text-ellipsis wd-sm">Timmothy Terry</td>
                    <td class="text-ellipsis">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris hendrerit nibh eu nisi congue dapibus.</td>
                    <td class="wd-xxs">
                      <!-- START dropdown-->
                      <div class="pull-right dropdown">
                        <button type="button" data-toggle="dropdown" class="btn btn-default btn-flat btn-flat-icon"><em class="ion-android-more-vertical"></em></button>
                        <ul role="menu" class="dropdown-menu md-dropdown-menu dropdown-menu-right">
                          <li><a href="#"><em class="ion-reply icon-fw"></em>Reply</a></li>
                          <li><a href="#"><em class="ion-forward icon-fw"></em>Forward</a></li>
                          <li><a href="#"><em class="ion-trash-a icon-fw"></em>Spam</a></li>
                        </ul>
                      </div>
                      <!-- END dropdown-->
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- Floating button for compose-->
            <div class="floatbutton">
              <ul class="mfb-component--br mfb-zoomin">
                <li class="mfb-component__wrap"><a id="compose" href="#" class="mfb-component__button--main"><i class="mfb-component__main-icon--resting ion-edit"></i><i class="mfb-component__main-icon--active ion-edit"></i></a>
                  <ul class="mfb-component__list"></ul>
                </li>
              </ul>
            </div>
            <!-- Modal content example for messages-->
            <div tabindex="-1" role="dialog" class="modal modal-right modal-auto-size fade modal-message">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <div class="media m0 pv">
                      <div class="pull-right">
                        <div data-dismiss="modal" class="clickable"><sup><em class="ion-close-round text-muted icon-2x"></em></sup></div>
                      </div>
                      <div class="initial48 bg-info pull-left">G</div>
                      <div class="media-body media-middle">
                        <h4 class="media-heading">Gail</h4><span class="text-muted">contato@gail.com.br</span>
                      </div>
                    </div>
                  </div>
                  <div class="modal-body">
                    <p class="text-muted">26 março 2017 10:30 am</p>
                    <h4 class="mt">Querido colaborador</h4>
                    <div class="reader-block">
                      <p>Praesent vel nisi nibh. Vestibulum purus ipsum, rutrum varius aliquam id, rhoncus eget neque. Curabitur sodales nisl eu enim suscipit eu faucibus dui mattis.</p>
                      <p>Aenean risus nulla, aliquam sed aliquam vitae, ultricies non elit. Suspendisse nunc massa, euismod eu egestas quis, molestie sit amet mauris. Mauris eu lacus massa, vel condimentum lectus. Quisque quam justo, cursus sit amet pretium vel, viverra vel leo. Nullam lobortis consectetur hendrerit. Aenean rhoncus, est vel vestibulum tristique, ante lectus elementum augue, eu dictum turpis dui ut ipsum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                      <p>Pellentesque ac ligula varius nisl laoreet pretium quis quis tellus. Praesent et mauris lacus, non volutpat augue.</p>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <div class="text-left">
                      <p>Reponder</p>
                    </div>
                    <div class="media m0 pv">
                      <div class="media-body media-middle">
                        <form action="">
                          <div class="mda-form-group">
                            <div class="mda-form-control pt0">
                              <textarea rows="3" aria-multiline="true" tabindex="0" aria-invalid="false" placeholder="Escreva aqui..." class="form-control"></textarea>
                              <div class="mda-form-control-line"></div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <button type="button" data-dismiss="modal" class="btn btn-info">Enviar</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Modal content example for messages-->
            <!-- Modal content example for compose-->
            <div tabindex="-1" role="dialog" class="modal fade modal-compose">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                    <form action="">
                      <div class="mda-form-group">
                        <div class="mda-form-control">
                          <input rows="3" aria-multiline="true" tabindex="0" aria-invalid="false" class="form-control">
                          <div class="mda-form-control-line"></div>
                          <label>To:</label>
                        </div>
                      </div>
                      <div class="mda-form-group">
                        <div class="mda-form-control">
                          <textarea rows="3" aria-multiline="true" tabindex="0" aria-invalid="false" class="form-control"></textarea>
                          <div class="mda-form-control-line"></div>
                          <label>Message</label>
                        </div>
                      </div>
                      <button type="button" data-dismiss="modal" class="btn btn-success">Send</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Modal content example for compose-->
          </div>
        </section>