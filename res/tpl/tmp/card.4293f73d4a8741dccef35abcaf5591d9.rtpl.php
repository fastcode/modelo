<?php if(!class_exists('raintpl')){exit;}?><!-- Page content-->
        <section>
          <div class="container container-fluid">
            <div class="row mv">
              <div class="col-sm-4">
                <a href="<?php echo $novo;?>"><button type="button" class="btn btn-info"><em class="ion-plus mr-sm"></em>Novo <?php echo $singular;?></button></a>
              </div>
              <div class="col-sm-8 text-right hidden-xs">
                <form class="form-inline">
                  <div class="form-group mr">
                    <label class="mr"><small>Status</small></label>
                    <select class="form-control input-sm">
                      <option>Todos</option>
                      <option>Finalizados</option>
                      <option>Abertos</option>
                      <option>Cancelados</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="mr"><small>Ordenar por</small></label>
                    <select class="form-control input-sm">
                      <option>Data</option>
                      <option>Nome</option>
                      <option>Valor</option>
                      <option>Status</option>
                    </select>
                  </div>
                </form>
              </div>
            </div>
            <div class="row">
              <!--Aberto-->
              <div class="col-md-6 col-lg-4">
                <!-- Project card-->
                <div class="card">
                  <div class="card-heading">
                    <div class="pull-right">
                      <div class="label label-<?php echo $cor;?>"><?php echo $status;?></div>
                    </div>
                    <div class="card-title"><?php echo $contato;?></div><small><?php echo $empresa;?></small>
                  </div>
                  <div class="card-body">
                    <p><strong>Descrição</strong></p>
                    <div class="pl-lg mb-lg"><?php echo $desc;?></div>
                    <p><strong>Última alteração</strong></p>
                    <div class="pl-lg mb-lg"><?php echo $dtalter;?></div>
                    <p><strong>Valor</strong></p>
                    <div class="pl-lg mb-lg"><?php echo $valor;?></div>
                    <p><strong>Endereço</strong></p>
                    <div class="pl-lg mb-lg"><?php echo $endereco;?></div>
                  </div>
                  <div class="card-footer">
                    <p><small><?php echo $footer;?></small></p>
                    <div class="progress progress-xs m0">
                      <div style="width:70%" class="progress-bar progress-bar-<?php echo $cor;?>"></div>
                    </div>
                  </div>
                </div>
                <!-- end Project card-->	 
              </div>
              <!--Finalizado-->
              <div class="col-md-6 col-lg-4">
                <!-- Project card-->
                <div class="card">
                  <div class="card-heading">
                    <div class="pull-right">
                      <div class="label label-info">Finalizado</div>
                    </div>
                    <div class="card-title">Junior Sampaio</div><small>Empresa J</small>
                  </div>
                  <div class="card-body">
                    <p><strong>Descrição</strong></p>
                    <div class="pl-lg mb-lg">Ut turpis urna, tristique sed adipiscing nec, luctus quis leo. Fusce nec volutpat ante.</div>
                    <p><strong>Última alteração</strong></p>
                    <div class="pl-lg mb-lg">15/03/2017 - 16:24</div>
                    <p><strong>Valor</strong></p>
                    <div class="pl-lg mb-lg">R$ 7.834,56</div>
                    <p><strong>Endereço</strong></p>
                    <div class="pl-lg mb-lg">Rua D, 67 - Centro - São Paulo - SP</div>
                  </div>
                  <div class="card-footer">
                    <p><small>Expira em 2 dias</small></p>
                    <div class="progress progress-xs m0">
                      <div style="width:70%" class="progress-bar progress-bar-info"></div>
                    </div>
                  </div>
                </div>
                <!-- end Project card-->
              </div>
              <!--Cancelado-->
              <div class="col-md-6 col-lg-4">
                <!-- Project card-->
                <div class="card">
                  <div class="card-heading">
                    <div class="pull-right">
                      <div class="label label-warning">Cancelado</div>
                    </div>
                    <div class="card-title">Junior Sampaio</div><small>Empresa J</small>
                  </div>
                  <div class="card-body">
                    <p><strong>Descrição</strong></p>
                    <div class="pl-lg mb-lg">Ut turpis urna, tristique sed adipiscing nec, luctus quis leo. Fusce nec volutpat ante.</div>
                    <p><strong>Última alteração</strong></p>
                    <div class="pl-lg mb-lg">15/03/2017 - 16:24</div>
                    <p><strong>Valor</strong></p>
                    <div class="pl-lg mb-lg">R$ 7.834,56</div>
                    <p><strong>Endereço</strong></p>
                    <div class="pl-lg mb-lg">Rua D, 67 - Centro - São Paulo - SP</div>
                  </div>
                  <div class="card-footer">
                    <p><small>Expira em 2 dias</small></p>
                    <div class="progress progress-xs m0">
                      <div style="width:70%" class="progress-bar progress-bar-warning"></div>
                    </div>
                  </div>
                </div>
                <!-- end Project card-->
              </div>
            </div>
          </div>
        </section>