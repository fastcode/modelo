<?php if(!class_exists('raintpl')){exit;}?><section>
  <div class="container-overlap bg-blue-500">
    <div class="media m0 pv">
      <div class="media-left pull-left"><a href="#"><img src="<?php echo $path;?>/res/img/user/01.jpg" alt="User" class="media-object img-default thumb128"></a></div>
      <div class="media-body media-middle pull-left">
        <h4 class="media-heading"><?php echo $nome;?></h4>
        <span class="text-muted">ID <?php echo $id;?></span><br>
        <span class="text-muted"><?php echo $funcao;?></span><br>
        <br>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form editable-form="" name="user.profileForm" class="card">
          <h5 class="card-heading pb0"> Sobre </h5>
          <div class="card-body">
            <p data-type="textarea" class="is-editable text-inherit"><?php echo $desc;?></p>
          </div>
          <div class="card-divider"></div>
          <div class="card-offset">
            <div class="card-offset-item text-right">
              <button id="edit-enable" type="button" class="btn-raised btn btn-warning btn-circle btn-lg"><em class="ion-edit"></em></button>
              <button id="edit-disable" type="submit" class="btn-raised btn btn-success btn-circle btn-lg hidden"><em class="ion-checkmark-round"></em></button>
            </div>
          </div>
          <h5 class="card-heading pb0">Informações básicas</h5>
          <div class="card-body">
            <table class="table table-striped">
              <tbody>
                <tr>
                  <td><em class="ion-egg icon-fw mr"></em>Data de nascimento</td>
                  <td><span data-type="date" data-mode="popup" class="is-editable text-inherit"><?php echo $dtnasc;?></span></td>
                </tr>
                <tr>
                  <td><em class="ion-ios-body icon-fw mr"></em>Admissão</td>
                  <td><span data-type="date" data-mode="popup" class="is-editable text-inherit"><?php echo $admissao;?></span></td>
                </tr>
                <tr>
                  <td><em class="ion-email icon-fw mr"></em>Email</td>
                  <td><span class="is-editable text-inherit"><a href="#"><?php echo $email;?></a></span></td>
                </tr>
                <tr>
                  <td><em class="ion-ios-telephone icon-fw mr"></em>Telefone</td>
                  <td><span class="is-editable text-inherit"><?php echo $fone;?></span></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="card-divider"></div>
          <h5 class="card-heading pb0">Informações Técnicas</h5>
          <div class="card-body">
            <table class="table table-striped">
              <tbody>
                <tr>
                  <td><em class="ion-android-person icon-fw mr"></em>Identificação</td>
                  <td><?php echo $id;?></td>
                </tr>
                <tr>
                  <td><em class="ion-ribbon-a icon-fw mr"></em>Função</td>
                  <td><?php echo $funcao;?></td>
                </tr>
                <tr>
                  <td><em class="ion-compass icon-fw mr"></em>Região</td>
                  <td><span data-type="date" data-mode="popup" class="is-editable text-inherit"><?php echo $regiao;?></span></td>
                </tr>
                <tr>
                  <td><em class="ion-pie-graph icon-fw mr"></em>Alçada</td>
                  <td><span data-type="date" data-mode="popup" class="is-editable text-inherit"><?php echo $alcada;?></span></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="card-footer">
            <label class="mda-radio">
              <input type="radio" name="otherradio" checked="" disabled="">
              <em class="bg-info"></em>Ativo </label>
            <label class="mda-radio">
              <input type="radio" name="otherradio" disabled="">
              <em class="bg-white"></em><span class="muted">Supervisor </span> </label>
            <label class="mda-radio">
              <input type="radio" name="otherradio" disabled="">
              <em class="bg-white"></em>Aprovador </label>
          </div>
        </form>
        <div class="card">
          <h5 class="card-heading pb0">Personalização do sistema</h5>
          <div class="card-body">
            <div class="container container-lg">
              <h5 class="text-center">Logomarca<br>
                <small>Selecione a imagem ou arraste e solte para área abaixo.</small></h5>
              <form id="dropzone-area" action="#" class="well dropzone">
                <div class="dropzone-previews"></div>
                <button type="submit" class="btn btn-primary pull-right">Encontrar</button>
              </form>
              <div class="form-group">
                <div class="text-center">
                  <p class="m0 ">Imprimir logomarca no orçamento?</p>
                  <label class="radio-inline c-radio">
                    <input id="inlineradio1" type="radio" name="i-radio" value="option1" >
                    <span class="ion-record"></span> Sim </label>
                  <label class="radio-inline c-radio">
                    <input id="inlineradio2" type="radio" name="i-radio" value="option2" >
                    <span class="ion-record"></span> Não </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card">
          <h5 class="card-heading pb0">Alteração de senha</h5>
          <div class="card-body">
            <div class="form-group">
              <div class="row">
                <div class="col-md-2 col-md-offset-3 col-sm-4 text-right">
                  <p class="m0 ">Senha atual</p>
                </div>
                <div class="col-sm-3">
                  <input type="password" class="form-control">
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-2 col-md-offset-3 col-sm-4 text-right">
                  <p class="m0 ">Nova senha</p>
                </div>
                <div class="col-sm-3">
                  <input id="id-source" type="password" name="match1" class="form-control">
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-2 col-md-offset-3 col-sm-4 text-right">
                  <p class="m0 ">Confirme a nova senha</p>
                </div>
                <div class="col-sm-3">
                  <input type="password" name="confirm_match" class="form-control">
                </div>
              </div>
            </div>
            <div class="col-md-5 col-md-offset-3 col-sm-4 text-right">
                        <button type="submit" class="btn btn-primary">Alterar</button>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
