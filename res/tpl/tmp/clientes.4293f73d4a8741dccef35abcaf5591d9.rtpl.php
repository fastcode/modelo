<?php if(!class_exists('raintpl')){exit;}?><section>
          <div class="container-fluid">
            <!-- DATATABLE DEMO 1-->
            <div class="card">
              <div class="card-heading">
            <div class="col-lg-8 col-md-8 col-sm-8">
              <div class="card-title"><?php echo $head_title;?></div>
              <div class="text-muted"></div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4"> <a href="#">
              <button type="button" class="fw btn btn-default ripple text-info"><strong><em class="ion-plus-circled"></em> Adicionar cliente</strong></button>
              </a> </div>
            <br><br>
          </div>
              <div class="card-body">
                <table id="datatable1" class="table-datatable table table-striped table-hover mv-lg">
                  <thead>
                    <tr>
                      <th class="sort-numeric">CNPJ</th>
                      <th class="sort-alpha">Razão Social</th>
                      <th class="sort-alpha">Responsável</th>
                      <th class="sort-numeric">Tipo</th>
                      <th class="sort-alpha">Local</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="gradeX">
                      <td>12345678912</td>
                      <td>Empresa X</td>
                      <td>Antonio</td>
                      <td>PJ</td>
                      <td>São Paulo/SP</td>
                    </tr>
                    <tr class="gradeC">
                      <td>543234432543</td>
                      <td>Empresa Y</td>
                      <td>Renata</td>
                      <td>PJ</td>
                      <td>Guarulhos/SP</td>
                    </tr>
                    <tr class="gradeA">
                      <td>878432843789</td>
                      <td>Empresa W</td>
                      <td>Alvaro</td>
                      <td>PJ</td>
                      <td>Mogi/SP</td>
                    </tr>
                    <tr class="gradeX">
                      <td>12345678912</td>
                      <td>Empresa X</td>
                      <td>Antonio</td>
                      <td>PJ</td>
                      <td>São Paulo/SP</td>
                    </tr>
                    <tr class="gradeC">
                      <td>543234432543</td>
                      <td>Empresa Y</td>
                      <td>Renata</td>
                      <td>PJ</td>
                      <td>Guarulhos/SP</td>
                    </tr>
                    <tr class="gradeA">
                      <td>878432843789</td>
                      <td>Empresa W</td>
                      <td>Alvaro</td>
                      <td>PJ</td>
                      <td>Mogi/SP</td>
                    </tr>
                    <tr class="gradeX">
                      <td>12345678912</td>
                      <td>Empresa X</td>
                      <td>Antonio</td>
                      <td>PJ</td>
                      <td>São Paulo/SP</td>
                    </tr>
                    <tr class="gradeC">
                      <td>543234432543</td>
                      <td>Empresa Y</td>
                      <td>Renata</td>
                      <td>PJ</td>
                      <td>Guarulhos/SP</td>
                    </tr>
                    <tr class="gradeA">
                      <td>878432843789</td>
                      <td>Empresa W</td>
                      <td>Alvaro</td>
                      <td>PJ</td>
                      <td>Mogi/SP</td>
                    </tr>
                    <tr class="gradeX">
                      <td>12345678912</td>
                      <td>Empresa X</td>
                      <td>Antonio</td>
                      <td>PJ</td>
                      <td>São Paulo/SP</td>
                    </tr>
                    <tr class="gradeC">
                      <td>543234432543</td>
                      <td>Empresa Y</td>
                      <td>Renata</td>
                      <td>PJ</td>
                      <td>Guarulhos/SP</td>
                    </tr>
                    <tr class="gradeA">
                      <td>878432843789</td>
                      <td>Empresa W</td>
                      <td>Alvaro</td>
                      <td>PJ</td>
                      <td>Mogi/SP</td>
                    </tr>
                    <tr class="gradeX">
                      <td>12345678912</td>
                      <td>Empresa X</td>
                      <td>Antonio</td>
                      <td>PJ</td>
                      <td>São Paulo/SP</td>
                    </tr>
                    <tr class="gradeC">
                      <td>543234432543</td>
                      <td>Empresa Y</td>
                      <td>Renata</td>
                      <td>PJ</td>
                      <td>Guarulhos/SP</td>
                    </tr>
                    <tr class="gradeA">
                      <td>878432843789</td>
                      <td>Empresa W</td>
                      <td>Alvaro</td>
                      <td>PJ</td>
                      <td>Mogi/SP</td>
                    </tr>
                    <tr class="gradeX">
                      <td>12345678912</td>
                      <td>Empresa X</td>
                      <td>Antonio</td>
                      <td>PJ</td>
                      <td>São Paulo/SP</td>
                    </tr>
                    <tr class="gradeC">
                      <td>543234432543</td>
                      <td>Empresa Y</td>
                      <td>Renata</td>
                      <td>PJ</td>
                      <td>Guarulhos/SP</td>
                    </tr>
                    <tr class="gradeA">
                      <td>878432843789</td>
                      <td>Empresa W</td>
                      <td>Alvaro</td>
                      <td>PJ</td>
                      <td>Mogi/SP</td>
                    </tr>
                    <tr class="gradeX">
                      <td>12345678912</td>
                      <td>Empresa X</td>
                      <td>Antonio</td>
                      <td>PJ</td>
                      <td>São Paulo/SP</td>
                    </tr>
                    <tr class="gradeC">
                      <td>543234432543</td>
                      <td>Empresa Y</td>
                      <td>Renata</td>
                      <td>PJ</td>
                      <td>Guarulhos/SP</td>
                    </tr>
                    <tr class="gradeA">
                      <td>878432843789</td>
                      <td>Empresa W</td>
                      <td>Alvaro</td>
                      <td>PJ</td>
                      <td>Mogi/SP</td>
                    </tr>
                    <tr class="gradeX">
                      <td>12345678912</td>
                      <td>Empresa X</td>
                      <td>Antonio</td>
                      <td>PJ</td>
                      <td>São Paulo/SP</td>
                    </tr>
                    <tr class="gradeC">
                      <td>543234432543</td>
                      <td>Empresa Y</td>
                      <td>Renata</td>
                      <td>PJ</td>
                      <td>Guarulhos/SP</td>
                    </tr>
                    <tr class="gradeA">
                      <td>878432843789</td>
                      <td>Empresa W</td>
                      <td>Alvaro</td>
                      <td>PJ</td>
                      <td>Mogi/SP</td>
                    </tr>
                    <tr class="gradeX">
                      <td>12345678912</td>
                      <td>Empresa X</td>
                      <td>Antonio</td>
                      <td>PJ</td>
                      <td>São Paulo/SP</td>
                    </tr>
                    <tr class="gradeC">
                      <td>543234432543</td>
                      <td>Empresa Y</td>
                      <td>Renata</td>
                      <td>PJ</td>
                      <td>Guarulhos/SP</td>
                    </tr>
                    <tr class="gradeA">
                      <td>878432843789</td>
                      <td>Empresa W</td>
                      <td>Alvaro</td>
                      <td>PJ</td>
                      <td>Mogi/SP</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>