<?php if(!class_exists('raintpl')){exit;}?><!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
	<meta http-equiv="Cache-control" content="public">
	<META HTTP-EQUIV="Expires" CONTENT="-1">
	<title><?php echo $head_title;?></title>
	<meta name="description" content="<?php echo $head_description;?>" />
	<meta name="keywords" content="<?php echo $head_keywords;?>" />
	<meta name="author" content="fasters.com.br" />
	<meta property="og:type" content="website">
	<meta property="og:locale" content="pt_BR">
	<meta property="og:url" content="<?php echo $head_url;?>">
 	<meta property="og:title" content="<?php echo $head_title;?>">
	<meta property="og:description" content="<?php echo $head_description;?>">
	<link rel="canonical" href="<?php echo $url;?>" />
	<link rel="shortcut icon" href="favicon.ico">
	
	<link rel="stylesheet" href="<?php echo $path;?>res/css/style.css" media="all" />
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/font-awesome/css/font-awesome.min.css">  
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/bootstrap/dist/css/bootstrap.min.css" media="all" />
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/animate.css/animate.min.css">
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/owl/dist/assets/owl.carousel.min.css" media="all" />
    <link rel="stylesheet" href="<?php echo $path;?>res/vendors/alertify/css/alertify.min.css" />
	<!--[if lt IE 9]>
	  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<SCRIPT TYPE="text/javascript">
		var initJQ = [];
	</SCRIPT>
</head>

<body>

	<div id="topo">
		topo
	</div>

	<div id="content">