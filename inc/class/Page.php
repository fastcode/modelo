<?php
class Page {
  	
	private $language;
	private $Tpl;
  	
	public $options = array(
		"data"=>array(
			"meta_author"=>"fasters.com.br",
			"url"=>"http://animallis.com.br",
			"head_description"=>"",
			"head_keywords"=>"",
			"head_url"=>"",
			"head_title"=>"",
			"head_description"=>"",
		)
	);
 
	public function __construct($options = array()){

		$rootdir = PATH;

		raintpl::configure("base_url", $rootdir );
		raintpl::configure("tpl_dir", $rootdir."/res/tpl/" );
		raintpl::configure("cache_dir", $rootdir."/res/tpl/tmp/" );
		raintpl::configure("path_replace", false );

		$options = array_merge($this->options["data"], $options);

		$this->language = new Language();

		$options['string'] = $this->language->loadString();
		$options['path'] = "/gail/";

		$tpl = $this->getTpl();
		$this->options = $options;
 
		if(gettype($this->options)=='array'){
			foreach($this->options as $key=>$val){
				$tpl->assign($key, $val);
			}
		}
 		
		$tpl->draw("header", false);
 
	}

	public function getString($name){

		return $this->language->getString($name);

	}
 
	public function __destruct(){
 
		$tpl = $this->getTpl();
 	
		if(gettype($this->options)=='array'){
			foreach($this->options as $key=>$val){
				$tpl->assign($key, $val);
			}
		}

		$tpl->draw("footer", false);
 
	}
 
	public function setTpl($tplname, $data = array(), $returnHTML = false){
 
		$tpl = $this->getTpl();
 
		if(gettype($data)=='array'){
			foreach($data as $key=>$val){
				$tpl->assign($key, $val);
			}
		}
 
		return $tpl->draw($tplname, $returnHTML);
 
	}
  
	public function getTpl(){
 
		return ($this->Tpl)?$this->Tpl:$this->Tpl = new RainTPL;
 
	}
 
}
?>
