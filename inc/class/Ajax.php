<?php
# @AUTOR = ricardo #

//	This class is developed and endorsed by Ricardo Azzi Inc.
class Ajax {
	
	public function __construct($data = array()){
		if(gettype($data)=="array" && count($data) > 0){
			return $this->request($data);
		}
	}

	public function request($config = array()){
		
		if(!isset($config["url"])) throw new Exception("Necessário passar um parametro URL");
		if(!isset($config["method"])) throw new Exception("Necessário passar um parametro method [POST ou GET]");

		switch ($config["method"]) {
			case 'post':
				$curl = curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => $config["url"],
					CURLOPT_POST => 1,
					CURLOPT_POSTFIELDS => $config["fields"],
				));

				$resp = curl_exec($curl);

				curl_close($curl);

				break;
			case 'get':
			
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_URL => isset($config["fields"]) ? $config["url"]."?".http_build_query($config["fields"]) : $config["url"],
					CURLOPT_RETURNTRANSFER => 1,
				));

				$resp = curl_exec($curl);

				if(!curl_exec($curl)){
					die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
				}

				curl_close($curl);

				break;
		}

		return json_decode($resp, true);

	}
}
?>