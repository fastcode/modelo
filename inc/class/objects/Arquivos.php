<?php

class Arquivos extends Model  {

    public $required = array("idarquivo");
    protected $pk = "idarquivo";
    
    public function get($id){
        return $this->queryToAttr("sp_arquivos_get $id");
    }
    
        
    public function load(){
        return $this->getSql()->arrays("sp_arquivos_load");
    }
        
    
    public function save(){
        $return = $this->getSql()->arrays("sp_arquivos_save ?,?,?);", array(
            $this->getidarquivo(),
$this->getdesarquivo(),
$this->getdescaminho()
        ));
        $this->get($return["idarquivo"]);
    }
    
    
    public function remove(){
        if($this->getidarquivo()){
            $this->getSql()->query("sp_arquivos_remove ?, ?;", array(
                0,
                $this->getidarquivo(),
            ));
        }
    }
    
        
}

?>