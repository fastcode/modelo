<?php
function autoload_php_default_project($class){

	

	$class = str_replace('\\', DIRECTORY_SEPARATOR, $class);

	

	$filepath = __DIR__.DIRECTORY_SEPARATOR."class".DIRECTORY_SEPARATOR.$class.".php";

	


    if(file_exists($filepath)){
    	require_once($filepath);
    	return true;
    }
    $filepath = __DIR__.DIRECTORY_SEPARATOR."class".DIRECTORY_SEPARATOR."objects".DIRECTORY_SEPARATOR.$class.".php";
	
    if(file_exists($filepath)){
    	require_once($filepath);
    	return true;
    }
	
    $filepath = __DIR__.DIRECTORY_SEPARATOR."class".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR.$class.".php";

    if(file_exists($filepath)){

    	require_once($filepath);
    	return true;

    }elseif(file_exists(__DIR__.DIRECTORY_SEPARATOR."class".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php")){

    	require_once(__DIR__.DIRECTORY_SEPARATOR."class".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
    	return true;

    }else{

    	throw new Exception("Classe não encontrada em: ".$filepath, 400);	

    }
}
if(URL === "/inc/configuration.php"){
	phpinfo();
	exit;
}
if(!function_exists('removeSimplesQuotes')){
	function removeSimplesQuotes($val){
		return str_replace("'", "", $val);
	}
}
if(!function_exists('request')){
	function request($key){
		return (isset($_REQUEST[$key]))?removeSimplesQuotes($_REQUEST[$key]):NULL;
	}
}
if(!function_exists('r')){
	function r($key){
		return request($key);
	}
}
if(!function_exists('post')){
	function post($key){
		return (isset($_POST[$key]))?removeSimplesQuotes($_POST[$key]):NULL;
	}
}
if(!function_exists('p')){
	function p($key){
		return post($key);
	}
}
if(!function_exists('get')){
	function get($key){
		return (isset($_GET[$key]))?removeSimplesQuotes($_GET[$key]):NULL;
	}
}
if(!function_exists('g')){
	function g($key){
		return get($key);
	}
}
if(!function_exists('in')){
	function in($t, $arrays = true, $keyEncode=''){
		if(is_array($t)){
			if($arrays){
				$b = array();
				foreach($t as $i){
					$n = array();
					foreach($i as $k=>$v){
						$n[chgName($k)] = $v;
					}
					$n['_'.chgName($keyEncode)] = in($i[$keyEncode]);
					array_push($b, $n);
				}
				return $b;
			}else{
				$n = array();
				foreach($t as $k=>$v){
					$n[chgName($k)] = $v;
				}
				$n['_'.chgName($keyEncode)] = in($t[$keyEncode]);
				return $n;
			}
		}else{
			$encode = base64_encode(time());
			return base64_encode(str_pad(strlen($encode), 3, '0', STR_PAD_LEFT).$encode.base64_encode($t));
		}
	}
}
if(!function_exists('out')){
	function out($t){
		$t = base64_decode($t);	
		$len = substr($t, 0, 3);
		return requestFIT(base64_decode(substr($t, ($len+3), strlen($t)-($len+3))));
	}
}
if(!function_exists('pre')){
	function pre(){
		echo "<pre>";
		foreach(func_get_args() as $arg){
			print_r($arg);
		}
		echo "</pre>";
	}
}
if(!function_exists('getClass')){
	function getClass($class_name){
		if(isset($_SESSION[$class_name])){
			return new $class_name($_SESSION[$class_name]);
		}else{
			throw new Exception("A classe $class_name não está em sessão");
		}
	}
}
if(!function_exists('setClass')){
	function setClass($class){
		$_SESSION[get_class($class)] = $class->getFields();
	}
}
if(!function_exists('array_sort_by_column')){
	function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
	    $sort_col = array();
	    foreach ($arr as $key=> $row) {
	        $sort_col[$key] = $row[$col];
	    }

	    array_multisort($sort_col, $dir, $arr);
	}
}
if(!function_exists('abreviateTotalCount')){
	function abreviateTotalCount($value){

	    $abbreviations = array(12 => 't', 9 => 'b', 6 => 'm', 3 => 'k', 0 => '');

	    foreach($abbreviations as $exponent => $abbreviation) 
	    {

	        if($value >= pow(10, $exponent)) 
	        {

	            return round(floatval($value / pow(10, $exponent)),1).$abbreviation;

	        }

	    }

	}
}
if(!function_exists('ipinfo')){
	function ipinfo($ip = null){

		if($ip === null) $ip = $_SERVER['REMOTE_ADDR'];

		$result = file_get_contents('http://ipinfo.io/'.$ip.'/json');

		if(!$result){

			throw new Exception("Não foi possível conectar o servidor de IP.");

		}

		return json_decode($result, true);

	}
}
if(!function_exists('is_email')){
	function is_email($text){
		return (filter_var($text, FILTER_VALIDATE_EMAIL)===false)?false:true;
	}
}
if(!function_exists('is_ip')){
	function is_ip($text){
		return (filter_var($text, FILTER_VALIDATE_IP)===false)?false:true;
	}
}
define('KEY_ENCRYPT', pack('a16', 'PHP-DEFAULT-PROJECT-BY-JOAORANGEL'));
if(!function_exists('encrypt')){
	function encrypt($data = array()){
		return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, KEY_ENCRYPT, json_encode($data), MCRYPT_MODE_ECB));
	}
}
if(!function_exists('decrypt')){
	function decrypt($data){
		return json_decode(trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, KEY_ENCRYPT, base64_decode($data), MCRYPT_MODE_ECB)), true);
	}
}
if(!function_exists('send_email')){
	function send_email($options, $tplname = "blank", $data = array(""), $comTemplatePadrao = true){

		/************************************************************/
		if(isset($options['body'])) $data['body'] = $options['body'];

		raintpl::configure("base_url", PATH );
		raintpl::configure("tpl_dir", PATH."/res/tpl/email/" );
		raintpl::configure("cache_dir", PATH."/res/tpl/tmp/" );
		raintpl::configure("path_replace", false );

		$tpl = new RainTPL;

		if(gettype($data)=='array'){
			foreach($data as $key=>$val){
				$tpl->assign($key, $val);
			}
		}

		$body = "";
		if($comTemplatePadrao === true) $body .= $tpl->draw("header", true);
		$body .= $tpl->draw($tplname, true);
		if($comTemplatePadrao === true) $body .= $tpl->draw("footer", true);

		$options['body'] = $body;
		/************************************************************/
		require_once(PATH."/inc/sendgrid/sendgrid-php.php");
		/************************************************************/
		$sendgrid = new SendGrid("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");

		$email = new SendGrid\Email();
		$email
		    ->addTo($options["to"])    
		    ->addBcc('bruno@fastcode.com.br')    
		    ->setFrom($options["from"])
		    ->setFromName($options["fromname"])
		    ->setSubject($options["subject"])    
		    ->setHtml($body)
		;

		$sendgrid->send($email);
		
	}
}
if(!function_exists('setUrl')){
	function setUrl($name, $value, $url = NULL){

		if(!$url){
			$qs = $_SERVER['QUERY_STRING'];
		}else{
			$qs = end(explode('?', $url));
		}

		$params = explode('&', $qs);
		$paramsNew = array();

		foreach ($params as &$p) {
			
			$var = explode('=', $p);
				
			if($name !== $var[0] && $var[0]){
				array_push($paramsNew, $var[0].'='.$var[1]);
			}

		}

		if($value!==false) array_push($paramsNew, $name.'='.$value);

		$url = URL_REQUEST;

		$url = explode('?', $url);

		return $url[0].'?'.implode('&', $paramsNew);

	}
}
if(!function_exists('getObjectFromSession')){
	function getObjectFromSession($object){

		if(isset($_SESSION[$object])){
			return new $object($_SESSION[$object]);
		}else{
			return new $object();
		}

	}
}
if(!function_exists('setObjectInSession')){
	function setObjectInSession($object){
		return $_SESSION[get_class($object)] = $object->getFields();
	}
}
if(!function_exists('setLocalCookie')){
	function setLocalCookie($name, $data, $time){
		setcookie($name, encrypt($data), time()+$time, "/", $_SERVER['SERVER_NAME'], 1);
	}
}
if(!function_exists('getLocalCookie')){
	function getLocalCookie($name){
		if (!isset($_COOKIE[$name])) return false;
		$data = $_COOKIE[$name];
		if($data!==NULL && strlen($data)>0){
			return decrypt($data);
		}else{
			return false;
		}
	}
}
if(!function_exists('unsetLocalCookie')){
	function unsetLocalCookie($name){
		if(isset($_COOKIE[$name])) unset($_COOKIE[$name]);
		setcookie($name, null, -1, "/");
	}
}
if(!function_exists('success')){
	function success($data = array()){

		$json = json_encode(array_merge(array(
			'success'=>true,
			'delay'=>microtime(true)-START_EXECUTION
		), $data));

		if(isset($_GET['callback'])){
			return get('callback').'('.$json.')';
		}elseif(isset($_GET['jsonp'])){
			return get('jsonp').'('.$json.')';
		}else{
			return $json;
		}

	}
}
if(!function_exists('array_merge_recursive_distinct')){
function array_merge_recursive_distinct ( array &$array1, array &$array2 ){
  $merged = $array1;

  foreach ( $array2 as $key => &$value )
  {
    if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) )
    {
      $merged [$key] = array_merge_recursive_distinct ( $merged [$key], $value );
    }
    else
    {
      $merged [$key] = $value;
    }
  }

  return $merged;
}
}
function echoMenuHTML(){
    echo Menu::getMenuSession();
}

function create_class_save($proc, $pk, $columns){
        
        $virgulas = array();
        foreach($columns as $val){
            array_push($virgulas, "?");
        }
        
        return '
    
    public function save(){
        $return = $this->getSql()->arrays("'.$proc.' '.implode(",",$virgulas).');", array(
            '.implode(",\n",$columns).'
        ));
        $this->get($return["'.$pk.'"]);
    }
    ';
}

function create_class_get($proc){
        return '
    
    public function get($id){
        return $this->queryToAttr("'.$proc.' $id");
    }
    ';
}

function create_class_remove($proc, $pk){
        return '
    
    public function remove(){
        if($this->get'.$pk.'()){
            $this->getSql()->query("'.$proc.' ?, ?;", array(
                0,
                $this->get'.$pk.'(),
            ));
        }
    }
    ';
}

function create_class_begin($name, $pk){
        $model = '<?php

class '.ucfirst($name).' extends Model  {

    public $required = array("'.$pk.'");
    protected $pk = "'.$pk.'";';

        return $model;
}

function create_class_end(){
        $model = '
        
}

?>';
        return $model;
}

function print_load($proc){
        return '
        
    public function load(){
        return $this->getSql()->arrays("'.$proc.'");
    }
        ';
}

function create_proc_save($config){
	$sql = new Sql();
	$n = 'sp_'.$config["table"].'_save';
	if(count($sql->arrays("select * from sys.objects where name = '$n'")) == 0){
		$p = array();
		foreach($config["columns"] as $val){
			array_push($p, "@".$val["name"]." ".$val["tipo"]);
		}
		$q = 'CREATE PROCEDURE '.$n.'
		'.implode(",\n", $p).'
		AS
		IF(@'.$config["pk"].' = 0) 
			BEGIN
				INSERT tb_'.$config["table"].' ('.implode(",", $config["columnsName"]).') VALUES('.implode(",", $config["columnsVariables"]).')
			END
		ELSE
			BEGIN
				UPDATE tb_'.$config["table"].' SET '.implode(",", $config["columnsUpdate"]).' WHERE '.$config["pk"].' = @'.$config["pk"].'
			END
		';
		$sql->query($q);
	}
}

function create_proc_load($config){
	$sql = new Sql();
	$n = 'sp_'.$config["table"].'_load';
	if(count($sql->arrays("select * from sys.objects where name = '$n'")) == 0){
		$q = 'CREATE PROCEDURE '.$n.'
		AS
		SELECT '.implode(",",$config["columnsAll"]).' FROM tb_'.$config["table"].' WHERE instatus = 1
		';
		$sql->query($q);
	}
}

function create_proc_remove($config){
	$sql = new Sql();
	$n = 'sp_'.$config["table"].'_remove';
	if(count($sql->arrays("select * from sys.objects where name = '$n'")) == 0){
		$q = 'CREATE PROCEDURE '.$n.'
		@'.$config["pk"].' int
		AS
		UPDATE tb_'.$config["table"].' SET instatus = 0 WHERE '.$config["pk"].' = @'.$config["pk"].'
		';
		$sql->query($q);
	}
}

function create_proc_get($config){
	$sql = new Sql();
	$n = 'sp_'.$config["table"].'_get';
	if(count($sql->arrays("select * from sys.objects where name = '$n'")) == 0){
		$q = 'CREATE PROCEDURE '.$n.'
		AS
		SELECT '.implode(",",$config["columnsAll"]).' FROM tb_'.$config["table"].'
		';
		$sql->query($q);
	}
}
function create_rest_begin($config){
            return '<?php 

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->group("/'.$config["table"].'", function () use ($app) {
        ';
        }

        function create_rest_get($config){
            return '
    $app->get("[/{id}]", function ($req, $res, $args) use ($app) {
        try{
            if(count($args) > 0){
                $'.strtolower($config["table"]).' = new '.ucfirst($config["table"]).'((int)$args["id"]);
                echo json_encode(array("success"=>true, "msg"=>"Opção de lista feita com sucesso!", "'.strtolower($config["table"]).'"=>$'.strtolower($config["table"]).'->getFields()));
            } else {
                $'.strtolower($config["table"]).' = new '.ucfirst($config["table"]).'();
                echo json_encode(array("success"=>true, "msg"=>"Opção de lista feita com sucesso!", "'.strtolower($config["table"]).'"=>$'.strtolower($config["table"]).'->load()));
            }
        } catch(Exception $e){
            throw new Exception("Erro ao Listar.");
        }
    });';
        }

        function create_rest_post_put($config){
            return '
    $app->map(["POST", "PUT"],"", function ($req, $res, $args) use ($app) {
        try{
            $'.strtolower($config["table"]).' = new '.ucfirst($config["table"]).'();
            $'.strtolower($config["table"]).'->arrayToAttr($_POST);      
            $'.strtolower($config["table"]).'->save();
            echo json_encode(array("success"=>true, "msg"=>"Opção de inserção/alteração feita com sucesso!", "'.strtolower($config["table"]).'"=>$'.strtolower($config["table"]).'->getFields()));
        } catch(Exception $e){
            throw new Exception("Erro ao salvar.");
        }
    });';
        }

        function create_rest_remove($config){
            return '
    $app->delete("", function ($req, $res, $args) use ($app) {
        try{
            $'.strtolower($config["table"]).' = new '.ucfirst($config["table"]).'();
            $'.strtolower($config["table"]).'->arrayToAttr($_POST);      
            $'.strtolower($config["table"]).'->remove();
            echo json_encode(array("success"=>true, "msg"=>"Opção de inserção/alteração feita com sucesso!", "'.strtolower($config["table"]).'"=>$'.strtolower($config["table"]).'->getFields()));
        } catch(Exception $e){
            throw new Exception("Erro ao salvar.");
        }
    });';
        }

        function create_rest_end($config){
            return '
});

?>';
        }

        function removeAcentos($string, $slug = false) {
	  $string = strtolower($string);
	  // Código ASCII das vogais
	  $ascii['a'] = range(224, 230);
	  $ascii['e'] = range(232, 235);
	  $ascii['i'] = range(236, 239);
	  $ascii['o'] = array_merge(range(242, 246), array(240, 248));
	  $ascii['u'] = range(249, 252);
	  // Código ASCII dos outros caracteres
	  $ascii['b'] = array(223);
	  $ascii['c'] = array(231);
	  $ascii['d'] = array(208);
	  $ascii['n'] = array(241);
	  $ascii['y'] = array(253, 255);
	  foreach ($ascii as $key=>$item) {
	    $acentos = '';
	    foreach ($item AS $codigo) $acentos .= chr($codigo);
	    $troca[$key] = '/['.$acentos.']/i';
	  }
	  $string = preg_replace(array_values($troca), array_keys($troca), $string);
	  // Slug?
	  if ($slug) {
	    // Troca tudo que não for letra ou número por um caractere ($slug)
	    $string = preg_replace('/[^a-z0-9]/i', $slug, $string);
	    // Tira os caracteres ($slug) repetidos
	    $string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
	    $string = trim($string, $slug);
	  }
	  return $string;
	}
?>